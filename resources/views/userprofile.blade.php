@extends('layouts.master')

@section('title')
    {{$user->username}}'s Profile
@endsection

@section('content')
    <!--Display any error or custom error/success messages if there are any -->
    @if(count($errors)>0)
        <div class="col-md-12 error">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(Session::has('message'))
        <div class="col-md-12 success">
            {{Session::get('message')}}
        </div>
    @endif
    <section class="row userfields">
        <div class="col-md-12 posts" style="padding-top: 10px; margin-top: 10px;">
            <header>
                <h3 class="col-md-12" style="text-align: center; margin-top: 15px; margin-bottom: 15px;">{{$user->username}}'s Profile</h3>
            </header>
            <!--Users details in a form so they can be updated if necessary (cannot change password or user id)-->
            <form action="{{route('userupdate')}}"  method="post" class="col-md-12">
                <div class="col-md-4" style="float: left">
                    <div class="form-group">
                        <label for="firstname">Username</label>
                        <input class="form-control" type="text" name = "username" id="username" value="{{$user->username}}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" type="email" name = "email" id="email" value="{{$user->email}}">
                    </div>

                </div>
                <div class="col-md-4" style="float:left;">
                    <div class="form-group">
                        <label for="lastname">Firstname</label>
                        <input class="form-control" type="text" name = "firstname" id="firstname" value="{{$user->firstname}}">
                    </div>
                    <div class="form-group">
                        <label for="lastname">Lastname</label>
                        <input class="form-control" type="text" name = "lastname" id="lastname" value="{{$user->lastname}}">
                    </div>
                </div>
                <div class="col-md-4" style="float:left;">
                    <div class="form-group">
                        <label for="username">User ID</label>
                        <input class="form-control" type="text" name = "userid" id="userid" readonly="readonly" value="{{$user->id}}">
                    </div>
                    <!--Radio button to set user permission disable buttons if logged in admin is looking at their profile do admin cannot change their own permission-->
                    <div class="form-group"><br>
                        <label for="email">Permission:&nbsp </label>
                        <input class="" type="radio" name = "permission" id="adminbut" value="1" @if($user->admin) checked="checked" @endif >Admin&nbsp
                        <input class="" type="radio" name = "permission" id="userbut" value="0" @if(!$user->admin) checked="checked" @endif @if(Auth::user()->id == $user->id)disabled = "disabled"@endif>User&nbsp
                    </div>
                </div>
                <button type="submit" class="btn btn-primary col-md-12">Save Changes</button>
                <input type="hidden" value ="{{Session::token()}}" name="_token">
            </form>
        </div>
    </section>
    <section class="row">
        <div class="col-md-12 posts" style="margin-top: 10px;">
            <header>
                <h3 class="col-md-12" style="text-align: center; margin-top: 15px; margin-bottom: 15px;" >{{$user->username}}'s Posts</h3>
            </header>
            <!--Displays all of user's posts-->
            @foreach($posts as $post)
                <article class="post">
                    <p>{{ $post->body }}</p>
                    <div class="info">
                        Posted by {{ $post->user->firstname }} at {{ $post->created_at }}
                    </div>
                </article>
            @endforeach
        </div>
    </section>
@endsection
