@extends('layouts.master')

@section('title')
    Home
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6" style="margin-top: 25px; margin-bottom: 15px; vertical-align: center; border-right: solid; border-width: 0.1px; border-color: #adbdeb ">
            <!--Display any error or custom error/success messages if there are any -->
            @if(count($errors)>0)
                <div class="error">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('message'))
                <div class="success">
                    {{Session::get('message')}}
                </div>
            @endif
        <!--Form to create new post-->
            <form action="{{route('createpost')}}" method="post" style= "padding-right: 25px; margin-top: 10px;">
                <header>
                    <h3>Share Your Thoughts {{Auth::user()->username}}</h3>
                </header>
                <div class="form-group">
                    <textarea class="form-control" name="message" id="newpost" rows="5" placeholder="Your Thoughts"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Post</button>
                <input type="hidden" value ="{{Session::token()}}" name="_token">
            </form>
        </div>

        <div class="col-md-6 posts" style="margin-top: 35px; ">
            <header>
                <h3>Everybodys Thoughts...</h3>
            </header>

            @foreach($posts as $post)
                <article class="post">
                    <p>{{ $post->body }}</p>
                    <div class="info">
                        Posted by {{ $post->user->firstname }} at {{ $post->created_at }}
                    </div>
                </article>
            @endforeach
        </div>
    </div>
@endsection