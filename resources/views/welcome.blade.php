@extends('layouts.master')

@section('title')
    Laravel Testsite
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6" style="padding-top: 25px; padding-bottom: 15px;">
            <form action="{{route('register')}}" method="post" style= "padding-right: 25px;border-right: solid; border-width: 0.1px; border-color: #adbdeb">
                <h3 style="text-align: center; margin-top: 15px; margin-bottom: 15px;">Register</h3>
                <!--Display any error or custom error/success messages if there are any -->
                @if(count($errors)>0)
                    @if($errors->has('login_username')||$errors->has('login_password'))
                    @else
                        <div class="error">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @endif
            <!--Register Form-->
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name = "username" id="username" value="{{Request::old('username')}}">
                </div>
                <div class="form-group">
                    <label for="firstname">Firstname</label>
                    <input class="form-control" type="text" name = "firstname" id="firstname" value="{{Request::old('firstname')}}">
                </div>
                <div class="form-group">
                    <label for="lastname">Lastname</label>
                    <input class="form-control" type="text" name = "lastname" id="lastname" value="{{Request::old('lastname')}}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="email" name = "email" id="email" value="{{Request::old('email')}}">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name = "password" id="password">
                </div>
                <div class="form-group">
                    <label for="passwordconf">Confirm Password</label>
                    <input class="form-control" type="password" name = "password_confirmation" id="password_confirmation">
                </div>
                <button type="submit" class="btn btn-primary">Register</button>
                <input type="hidden" name = "_token" value = "{{Session::token()}}">
            </form>
        </div>

        <div class="col-md-6" style="padding-top: 25px;">
            <form action="{{route('login')}}" method="post">
                <h3 style="text-align: center; margin-top: 15px; margin-bottom: 15px;">Login</h3>
                <!--Display any error or custom error/success messages if there are any -->
                @if(count($errors)>0)
                    @if($errors->has('login_username')||$errors->has('login_password'))
                        <div class="error">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @endif
                @if(Session::has('message'))
                    <div class="error">
                        {{Session::get('message')}}
                    </div>
            @endif
            <!--Login Form-->
                <div class="form-group">
                    <label for="login_username">Username</label>
                    <input class="form-control" type="text" name = "login_username" id="login_username" value="{{Request::old('login_username')}}">
                </div>
                <div class="form-group">
                    <label for="login_password">Password</label>
                    <input class="form-control" type="password" name = "login_password" id="login_password"  value="{{Request::old('login_password')}}">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
                <input type="hidden" name = "_token" value = "{{Session::token()}}">
            </form>
        </div>
    </div>
@endsection