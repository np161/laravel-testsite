@extends('layouts.master')

@section('extrascripts')
    <!--Additional Datable CSS/JS-->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="{{URL::to('js/main.js')}}"></script>
@endsection

@section('title')
    Admin Home
@endsection

@section('content')
    @if(Session::has('message'))
        <div class="col-md-12 success">
            {{Session::get('message')}}
        </div>
    @endif
    <!--Datatable HTML-->
    <div class="col-md-12" >
        <header>
            <h3 class="col-md-12" style="text-align: center; margin-top: 15px; margin-bottom: 15px;">User List</h3>
        </header>
        <table id="usertable" class="table-hover">
            <thead>
            <tr>
                <th>User ID</th>
                <th>Username</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>User Permission</th>
                <th>User Profile</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->username}}</td>
                    <td>{{$user->firstname}}</td>
                    <td>{{$user->lastname}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        @if($user->admin)
                            Admin
                        @else
                            User
                        @endif
                    </td>
                    <!--Button to view user's profile-->
                    <td><button type="button" class="btn btn-primary" onclick="window.location = '{{route('userprofile',['userid' => $user->id])}}'">View</button></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection