<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #2b4bab">
  <a class="navbar-brand" href="#" style="color: white">
    <img src="images/lts_logo.png" width="35" height="35" class="d-inline-block align-top" alt="">
    Laravel Testsite
  </a>

  <div class="navbar-collapse collapse" id="collapsingNavbar">
    <ul class="navbar-nav ml-auto">
      <!-- If user is logged in display logged out button-->
      @if(Auth::check())
      <li class="nav-item">
        <a class="nav-link" href="{{route('logout')}}" data-target="#myModal" data-toggle="modal">Logout {{Auth::user()->username}}</a>
      </li>
        @endif
    </ul>
  </div>
</nav>
