<!doctype html>
    <head>
	<!-- jQuery Script-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"/>
	<!-- Bootsttrap Script-->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	<!-- Bootstrap CSS Sheet-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous"/>
	<!-- Custom CSS Sheet-->
	<link rel="stylesheet" href="{{URL::to('css/main.css')}}">
	<!-- Additional CSS/JS can be inserted if necessary-->
		@yield('extrascripts')
		<title>@yield('title')</title>
    </head>
    <body>
	<!-- Insert Header component-->
    	@include('components.header')
	<div class="container">
		@yield('content')
	</div>
    </body>
</html>