<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller{

    /*
     * redirects to the admin homepage
     */
    public function getAdminDashboard(){
        $users = User::all();

        return view ('admindashboard', ['users' => $users]);
    }


    /*
     * Function for registering new users.
     *
     * User permission is user by default but
     * can be changed by admin user
     */
	public function postRegister(Request $request){
	    //form validation
        $this ->validate($request,[
            'username' => 'required|min:2|max:20|unique:users',
            'firstname' => 'required|min:2|max:50|',
            'lastname' => 'required|min:2|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4|max:20|confirmed',
            'password_confirmation' => 'required|min:4|max:20'
        ]);

        $username = $request['username'];
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $email = $request['email'];
        $password = $request['password'];

        $user = new User();

        $user -> username = $username;
        $user -> firstname = $firstname;
        $user -> lastname = $lastname;
        $user -> email = $email;
        $user -> password = bcrypt($password);
        $user -> admin = false;

        $user->save();

        //login user and redirect to user home page
        Auth::login($user);
        return redirect() -> route('userdashboard');
	}


	/*
	 * Login function for already registered users
	 */
	public function postLogin(Request $request){
	    //form validation
        $this ->validate($request,[
            'login_username' => 'required',
            'login_password' => 'required',
        ]);

        /*
         * Attempt to log the user in as well as checking the user permission.
         *
         * If user is admin redirect to admin homepage, if not then redirect
         * to user homepage
         */
        if (Auth::attempt(['username' => $request['login_username'], 'password' => $request['login_password']])){

            if(DB::table('users')->where('username',$request['login_username'])->value('admin')){
                return redirect()->route('admindashboard');
            }else{
                return redirect()->route('userdashboard');
            }
        }else{
            return redirect()-> back()->with(['message' => 'Username or Password Incorrect']);
        }
	}

    /*
     * Logout user funstion
     */
	public function getLogout(){
	    Auth::logout();

	    return redirect()->route('home');
    }

    /*
     * Admin function to go the profile page for a specific user
     */
	public function getUserProfile($userid){
	    //Retrieve information about user and all of that users posts
        //to be displayed on userprofile page
        $user = User::where('id', $userid)->first();
        $posts = Post::where('user_id', $userid)->get();


	    return view('userprofile', ['user' => $user, 'posts' => $posts]);
    }

    /*
     * Admin function to update information about a user
     * or change user permissions
     */
    public function postUserUpdate(Request $request){
        //form validation
        $this ->validate($request,[
            'username' => 'required|min:2|max:20',
            'firstname' => 'required|min:2|max:50|',
            'lastname' => 'required|min:2|max:50',
            'email' => 'required|email'
        ]);

	    $username = $request['username'];
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $email = $request['email'];
        $permission = $request['permission'];

        $user = User::find($request['userid']);

        $user -> username = $username;
        $user -> firstname = $firstname;
        $user -> lastname = $lastname;
        $user -> email = $email;
        $user -> admin = $permission;

        $user->update();

	    return redirect()->route('admindashboard')->with(['message' => 'Updated Successfully']);
    }
}