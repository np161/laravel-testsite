<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller{

    /*
     * returns the homepage for the user after logging in
     */
    public function getUserDashboard(){
        $posts =  Post::orderBy('created_at','desc')->get();

        return view ('userdashboard',['posts' => $posts]);
    }

    /*
     * create post and add it to the database and redirect
     * to updated homepage
     */
    public function postCreatePost(Request $request){
        //form validation
        $this->validate($request,[
           'message' => 'required|max:400'
        ]);

        $post = new Post();

        //custom error/success message
        $post->body = $request['message'];
        $message = 'Posting Error';


        if($request->user()->posts()->save($post)){
            $message = 'Posted Sucessfully';
        }
        return redirect()->route('userdashboard')->with(['message' => $message]);
    }
}