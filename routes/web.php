<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['middleware' => ['web']], function(){
    Route::get('/', function () {
        return view('welcome');
    })->name('home');

    Route::post('/register', [
        'uses' => 'UserController@postRegister',
        'as' => 'register'
    ]);

    Route::get('/logout',[
        'uses' => 'UserController@getLogout',
        'as' => 'logout'
    ]);

    Route::post('/login', [
        'uses' => 'UserController@postLogin',
        'as' => 'login'
    ]);

    Route::get('/userdashboard',[
        'uses' => 'PostController@getUserDashboard',
        'as' => 'userdashboard',
        'middleware' => 'auth'
    ]);

    Route::get('/admindashboard',[
        'uses' => 'UserController@getAdminDashboard',
        'as' => 'admindashboard',
        'middleware' => 'auth'
    ]);

    Route::post('/createpost', [
       'uses' => 'PostController@postCreatePost',
       'as' => 'createpost',
        'middleware' => 'auth'
    ]);

    Route::get('/userprofile/{userid}',[
       'uses' => 'UserController@getUserProfile',
       'as' => 'userprofile',
        'middleware' => 'auth'
    ]);

    Route::post('/userupdate', [
       'uses' => 'UserController@postUserUpdate',
       'as' => 'userupdate',
        'middleware' => 'auth'
    ]);
});